using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static event Action StartGame = delegate { };
    public static event Action OnCubeSpawned = delegate { };

    private CubeSpawner[] spawners;
    private int spawnerIndex;
    private CubeSpawner currentSpawner;
    private bool isStart;
    private bool isEnd;
    private void Awake()
    {
        spawners = FindObjectsOfType<CubeSpawner>();
        MovingCube.EndGame += EndGame;
    }

    private void EndGame()
    {
        isEnd = true;
    }
    public void Touch()
    {
        if (isEnd)
            SceneManager.LoadScene(0);
        if (MovingCube.CurrentCube != null)
            MovingCube.CurrentCube.Stop();

        spawnerIndex = spawnerIndex == 0 ? 1 : 0;
        currentSpawner = spawners[spawnerIndex];

        if (isEnd)
            return;
        currentSpawner.SpawnCube();
        OnCubeSpawned();
        if (!isStart)
            StartGame();
    }
}
