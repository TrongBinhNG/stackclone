using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreText : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] RectTransform textRect;
    private int score = -1;

    private void Start()
    {
        GameManager.OnCubeSpawned += GameManager_OnCubeSpawned;
        MovingCube.EndGame += EndGame;
    }

    private void EndGame()
    {
        textRect.anchoredPosition = Vector3.zero;
        text.text = "Tap To Restart";
    }

    private void OnDestroy()
    {
        GameManager.OnCubeSpawned -= GameManager_OnCubeSpawned;
        MovingCube.EndGame -= EndGame;
    }

    private void GameManager_OnCubeSpawned()
    {
        gameObject.SetActive(true);
        textRect.anchoredPosition = new Vector2(0, 400);
        score++;
        text.text = "Score: " + score;
    }
}
